# GIS2DJI
Exports GIS files to a simple kml compatible with DJI pilot.

## Description
GIS2DJI has been tested with the following file formats: gpkg, shp, mif, tab, geojson, gml, kml and kmz. GIS_2_DJI will scan every file, every layer and every geometry collection (ie: MultiPoints) and create one output kml or kmz for each object found. 

Output file naming convention is: shp2dji-gpkg-MP-0-1-Point.kmz
- Original file name --> "shp2dji"
- Original file extension --> "gpkg"
- Original layer (if file has layers) --> "MP"
- Source index number --> "0"
- Source multigeometry index (if is a multigeometry) --> "1"
- Geometry type --> "Point"
- .kml or kmz --> ".kmz"

It will import points, lines and polygons, and converted each object into a compatible DJI kml file. Lines and polygons will be exported as kml files.  Points will be converted as PseudoPoints.kml. A PseudoPoints fools DJI to import a point as it thinks it's a line with 0 length. This allows you to import points in mapping missions. Points will also be exported as Point.kmz because PseudoPoints are not visible in a GIS or in Google Earth. The .kmz file format should make points compatible with some DJI mission software.

## Installation
In Anaconda Python
Create a new Python Environment (3.10)
Install Geopandas -> https://geopandas.org/en/stable/getting_started/install.html
Install simplekml -> https://pypi.org/project/simplekml/
Install fiona and shapely if not installed by Geopandas 
Install customtkinter -> https://pypi.org/project/customtkinter/ or https://customtkinter.tomschimansky.com/

You can also install the Anaconda Python Environment using the GIS2DJI_v02_1_CondaEnvironment.yaml file.  Open the Anaconda Navigator -> Environments -> Import.

To make a Windows GUI:
Install pyinstaller -> https://www.pyinstaller.org/en/stable/installation.html  (If you intend you make a stand alone Windows Version)

In the Anaconda prompt, run the following command: pyinstaller --noconfirm --onedir --windowed --add-data "C:\Users\njaca\anaconda3\envs\geopandas\Lib\site-packages/customtkinter;customtkinter/"  "C:\Users\youruserpath\Dev\Python\SpyderProjects\Python3\kml\GIS2DJI_v02.py"

## Usage
Batch mode:
1: Start by selecting a file format if needed.
2: Select a string filter if needed.
3: Select an input then an output directory. They must be different directories.
4: Press "Process files button"
        
Single or multiple file mode:
1: Go to file --> Import --> Select one or multiple files. You can select a file format.  Live dangerously will show you all files, some formats may be compatible but have not been tested.
3: Go to file --> Export to select an output directory.  Must be different than the input file directory.
4: Press "Process files" button

## Roadmap

## Contributing
McGill Applied Remote Sensing Lab
ARSL
https://arsl.geog.mcgill.ca/

## Authors and acknowledgment
Nicolas Cadieux
njacadieux.gitlab@gmail.com
https://gitlab.com/njacadieux
https://www.youtube.com/@nicolascadieux5881

## License
GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

Cite:Cadieux, Nicolas (2024). GIS2DJI. figshare. Software. https://doi.org/10.6084/m9.figshare.25263037.v1
DOI: https://doi.org/10.6084/m9.figshare.25263037.v1

